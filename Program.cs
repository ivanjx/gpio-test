﻿using System;
using System.Device.Gpio;
using System.Threading.Tasks;

namespace GPIOTest
{
    class Program
    {
        const int BUZZER = 21;

        static void Main(string[] args)
        {
            Console.WriteLine("Starting...");
            
            using(GpioController controller = new GpioController())
            {
                controller.OpenPin(BUZZER, PinMode.Output);
                
                while (true)
                {
                    Console.WriteLine("HIGH");
                    controller.Write(BUZZER, PinValue.High);
                    Task.Delay(500).Wait();

                    Console.WriteLine("LOW");
                    controller.Write(BUZZER, PinValue.Low);
                    Task.Delay(500).Wait();
                }
            }
        }
    }
}
