# Download sdk and build.
FROM mcr.microsoft.com/dotnet/sdk:5.0.203-focal-arm64v8 as build
COPY . /build
WORKDIR /build
RUN dotnet publish \
    -c Release \
    -r linux-arm64 \
    -p:PublishSingleFile=true \
    -p:PublishReadyToRun=true \
    --self-contained false \
    -o output

# Download runtime and copy build result.
FROM mcr.microsoft.com/dotnet/runtime:5.0.6-focal-arm64v8 as runtime
COPY --from=build /build/output /app

# Entry point.
ENTRYPOINT [ "/app/GPIOTest" ]
